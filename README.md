# README #

### What is this repository for? ###

* This is a collection of my old perl utilities and convenience functions years ago and want to archive.
* Mostly they deal with tab-ish delimited files and with fast[qa] sequence files

### How do I get set up? ###

* Download the files; You'll have to put the *pm files in a directory in your PERL5LIB path. 

### Contribution guidelines ###

* This code base poorly documented and not supported; I doubt it'll evolve.