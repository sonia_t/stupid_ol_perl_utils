#!/usr/bin/perl -w
use strict;
use Getopt::Long;
my $helpStr="
 Some fasta headers(names) are listed in a file.
 Read that file,
 and grab only that subset of sequences from a fasta file.
 ( where certain parts of the headers/names have to match exactly )
 has poorly documented options --idCol --process_seqid --inverse
 and --altNameCol for adding a new alternative name 
";
our $VERSION = "v1.2";
# # # # # # # # # # OPTIONS # # # # # # # # # # 
my $debug = 0;
my $hasHeader =0;
my $print_not_found = 0;
my $inverseMatching = 0;
my $process_seqid =0;
my $idCol = 0;
my $altNameCol;
# # # # # # # # # # # # # # # # # # # # # # # # # 

# # # # # # # # # # # # # # # # 
# GET USER SUPPLIED PARAMETERS

GetOptions ( 
    'debug!'     => \$debug ,
    'hasHeader!' => \$hasHeader , 
    'inverse!'  => \$inverseMatching ,
    'process_seqid!' => \$process_seqid , 
    'print_not_found!'  => \$print_not_found ,
    'idCol:i' => \$idCol ,
    'altNameCol:i' => \$altNameCol ,
    ) ;

print STDERR join(" ",@ARGV)."\n" if $debug;
(@ARGV==3) or die "<seqs.fa> <subset_patterns_file> <outfile> ";
my ($infilefa, $infilesubset, $outfile) = @ARGV;


sub process_head {
    my $head = $_[0];
    $head =~ s/\/\d$//;  # gets rid of pe marker if need be 
    $head =~ s/\#.*//;  # gets rid of the barcode, too. 
    $head =~ s/\|.*//;
#    $head =~ s/_.*//;  # gets rid of the subseq coords
    return $head;
}

open my $SUBSETFILE, $infilesubset or die "Can't open $infilesubset: $!\n";
my %getheads=();
while (<$SUBSETFILE>) {
    chomp;
    s/^>//;
    my $head = (split)[$idCol];
    my $altName =  (split)[$altNameCol] if defined($altNameCol);
    print STDERR 'altName: ',$altName."\n" if $debug;
    $head =  $process_seqid? process_head($head) : $head;
    $getheads{$head} = defined($altName) ? $altName : $head ;
}
close $SUBSETFILE;

open my $FAFILE, $infilefa or die "Can't open $infilefa: $!\n";
open my $OUTFILE, "> $outfile" or die "can't open $outfile: $!";
my $satisfies = 0;
my $head;
my $processed_head;
while(<$FAFILE>) {
    chomp;
    if (/^>/) {
	$head = (split)[0];
	$head =~ s/^>//;
	$processed_head =  $process_seqid? process_head($head) : $head;
	$satisfies =  defined( $getheads{$processed_head} ) ? 1 : 0 ; 
    } 

    if ( $satisfies ne $inverseMatching ) {
	$_ = $_ . " " . $getheads{$processed_head} if defined($altNameCol) && /^>/;
	print STDERR join(" ", $head , $processed_head, $getheads{$processed_head})."\n" if $debug && 0;
	print $OUTFILE $_ ."\n";
    }
}
close $FAFILE;
close $OUTFILE;
