#!/usr/bin/perl 
our $VERSION = "v2";

use List::Util qw/ min max sum /;
sub median; sub n50; sub cumsum; sub mean;
sub contigs2MB; 
use strict;
# die "this is broken ,it just runs and runs...\n";

@ARGV==1 or die "supply contigs file";
my $infile = $ARGV[0];
my $outfile = $infile.".stats";
open my $OUTFILE, "> $outfile" or die "can't open outfile";

sub readFasta {
    open INFILE, "$_[0]" or die "can't open infile $_[0]: $!";
    my $header = "";
    my %seq;
    while(<INFILE>){
        chomp;
        if (/^>(\S+)/){
            $header = $1;
            next;
        }
        $seq{$header} .= $_;
    }
    return \%seq;
}
sub contigLengths {
    my %gen = %{$_[0]};
    my %len = ();
    map { $len{$_} = length($gen{$_}) } keys %gen;
    return \%len;
}

sub countLong {
    my %len = %{$_[0]};
    my $longlen;
    map { $longlen++ if $_ > 1000 } values %len;
    return $longlen;
}
my %contigLengths = %{contigLengths(readFasta($infile))};
my @lengths = sort {$b <=> $a} values %contigLengths;
my $longCount = countLong(\%contigLengths);
#my %m2numC = %{contigs2MB(@lengths)};
print {$OUTFILE} "## number of contigs:\t", @lengths+0, "\n";
print {$OUTFILE} "## number of contigs longer than 1000bp:\t", $longCount, "\n";
print {$OUTFILE} "## total contig length:\t", sum(@lengths), "\n"; 
print {$OUTFILE} "## max contig length:\t", max(@lengths), "\n";
print {$OUTFILE} "## median contig length:\t", median(@lengths), "\n";
print {$OUTFILE} "## N50:\t", n50(@lengths), "\n";
print {$OUTFILE} "## num contigs to get to milestones", join(": ", map{$_}( contigs2MB(@lengths)) ),"\n";
#print {$OUTFILE} "## num contigs to get to",join(@milestones), join(",",@{contigs2MB(@lengths)}). "\n";

my $cumsum=0;
map {
    $cumsum+=$contigLengths{$_}; 
    print {$OUTFILE} join("\t",$_,$contigLengths{$_},$cumsum)."\n";
} sort {$contigLengths{$b} <=> $contigLengths{$a}} keys %contigLengths; 

sub contigs2MB {
    my @sorted = @_;
    my @cumsumlengths = @{ cumsum(@sorted) } ;
#    print STDERR join( "," , @cumsumlengths)."\n";
    my @milestones= (1e6, 2e6, 3e6, 4e6, 5e6);
    my @numContigs2milestone;
    my %milestone2numContigs;
    my $milestones_string; my $when_reached_string;
    my $m =0; 
    my $c = 1;
    foreach my $milestone (@milestones) {
	last if $milestone > @cumsumlengths[-1];
	$milestones_string .= sprintf("%2g MB,", $milestone / 1e6) ;
	while ( $cumsumlengths[$c-1] < $milestone) {
	    $c++;
#	    print STDERR join(",",$c,  $cumsumlengths[$c] , @milestone)."\n";
	}
	$when_reached_string .= sprintf("%d,",$c);
	$milestone2numContigs{$milestone}=$c;
	push @numContigs2milestone, $c;
    }
#    print STDERR join("\t", $milestones_string, $when_reached_string)."\n" ;
#    return \%milestone2numContigs, 
    return ($milestones_string, $when_reached_string) ;
}

sub n50 {
    my @cl = @_;
    my @sorted = sort {$a <=> $b} @cl;
    my @cumsumlengths = @{ cumsum(@sorted) };
    my $halfLength = $cumsumlengths[-1] / 2;
    my $c = 0;
    while ($c < @cumsumlengths-1) {
	$c++;
	next if ($cumsumlengths[$c] < $halfLength); # need option for exact just in case
	last if ($cumsumlengths[$c] > $halfLength);
    }
    my $n50 = mean(@sorted[$c,$c+1]);
    return $n50;
}

sub mean {
    return sum(@_) / @_;
}


sub cumsum {
    my @vals = @_;
    my @cumsum = ($vals[0]);
    foreach my $v (@vals[1..@vals-1]) {
        push @cumsum, $cumsum[-1]+$v;
    }
    return \@cumsum;
}

sub median {
    my @all = @_;
    my $med;
    my @sorted = sort {$a <=> $b} @all;
    my $n = @sorted+0;
    if ($n==0) {return 0};
    if ($n%2==1) {
        $med = $sorted[($n-1) / 2];
    } else {
        $med = ($sorted[$n/2] + $sorted[$n/2 -1]) / 2;
    }
    return $med;
}

