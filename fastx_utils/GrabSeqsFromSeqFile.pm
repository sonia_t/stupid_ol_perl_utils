#!/usr/bin/perl -w

package GrabSeqsFromSeqFile;
use strict;
use Exporter;
use vars qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);

my @fxns = qw(
	      fafiles2subset
	      remove_pe_tag
	      remove_subseq_coords
	      read_heads
	      clean_head 
	      trim_head_to_word
	      remove_SHERA_stats
	      revcomp
	      readCoordsFile
	      readCoordsFile_gff
	      );

$VERSION     = 1.00;
@ISA         = qw(Exporter);
@EXPORT      = ();
@EXPORT_OK   = @fxns ;
%EXPORT_TAGS = ( DEFAULT => \@EXPORT_OK ,
                 Both    => \@EXPORT_OK ,
		 all     => \@EXPORT_OK , 
		 );

my $debug=0;
# Some fasta headers(names) are listed in a file. Read that file,
# and grab only that subset of sequences from a fasta file.
# ( where the headers/names have to match exactly )

my $docStr = ' Pass parameter hash 
file with list of heads and fafile(s) is mandatory; 
options include; head_col ne 0, inverse_matching, ignore_pe , remove_subseq_coords ';

sub fafiles2subset {
    print STDERR "if you feed in a illumina fastq, it should work - output will be fastq.\n";

    my %par = %{$_[0]};
#    my @infiles = @{$par{'fafiles'}};  # orig but said "fix"
    my @infiles = ( $par{'superset_file'} ) ||  @{$par{'fafiles'}};  # fix
    (my $outfilefa =  $par{'subset_file'} || $par{'outfile'}) || die "provide an outfile";
    open my $OUTFILE, "> $outfilefa" or die "Can't open $outfilefa: $!\n";

    my %heads_subset = %{$par{'heads_subset'}} or die "no heads subset given";
    my $inverse = $par{'inverse'} || 0;

    foreach my $infilefa (@infiles) {
	open my $FAFILE, $infilefa or die "Can't open $infilefa: $!\n";
	my $satisfies = 0;
	while(<$FAFILE>) {
	    chomp;
	    if (/^>/ or /^\@HWI/ ) {
		my $head = (split)[0];
		$head =~ s/^>//;
		$head =~ s/^\@//;
		$head = clean_head($head) if $par{'clean_head'} ;
		$head = trim_head_to_word($head) if $par{'trim_head_to_word'} ;
		$head = remove_pe_tag($head) if $par{'remove_pe_tag'} ;
		$satisfies =  exists( $heads_subset{$head} ) ? 1 : 0 ; 
	    }
	    if ( $satisfies ne $inverse ) {
		print $OUTFILE $_ ."\n";
	    }
	}
	close $FAFILE;
    }
    return 1;
}
sub trim_head_to_word {
    my $head = $_[0];
    $head = (split /\s+/,$head)[0] ;
    return $head;
}
sub remove_pe_tag {
    my $head = $_[0];
    $head =~ s/\/[123]$//;  # gets rid of pe marker if need be 
    return $head;
}
sub remove_SHERA_stats {
    my $head = $_[0];
    $head =~ s/_\d+bp_.*//;  # not really specific enough? ... better now ? 
    return $head;
}
sub clean_head {
    my $head = $_[0];
    print STDERR "head before: $head\n" if $debug;
    $head =~ s/^\>//g;
    $head =~ s/^\@//g;
    $head =~ s/[\#\/].*//;  # gets rid of the barcode, and pe tag
    print STDERR "head after: $head\n" if $debug;
    return $head;
}

sub remove_subseq_coords {
    my $head = $_[0];
    $head =~ s/_\d+_\d+$//;  # gets rid of the subseq coords
    return $head;
}

sub read_heads { # this could definitely be folded into a more general fxn
    my %par= %{$_[0]};
    my $infile = $par{'heads_file'};
    my $col = $par{'heads_col'} || 0; 
    open my $SUBSETFILE, $infile or die "Can't open $infile: $!\n";
    my %getheads=();
    while (<$SUBSETFILE>) {
	next unless /\w/;
	chomp;
	s/^>//;
	my $head = (split)[$col];
	$head = remove_pe_tag($head) if $par{'remove_pe_tag'};
	$head = clean_head($head) if $par{'clean_head'} ;
	$getheads{$head} = 1 ;
    }
    close $SUBSETFILE;
    return \%getheads;
}

sub readCoordsFile {
    open my $INFILE, "$_[0]" or die "can't open infile $_[0]: $!";
    my %par = $_[1] ? %{$_[1]} : ();
    my $delimiter = defined( $par{'delimiter'}) ? $par{'delimiter'} : "\t";
    my $cols_str = defined($par{'cols_str'}) ? $par{'cols_str'} : '0,1,2';
    my @cols = split(',', $cols_str);
#    print STDERR join(" ","coords cols from readCoordsFile:", @coords_cols)."\n";
    my %regions;
    while(<$INFILE>){
        chomp;
        next unless /\w/;
        my ($cs, $b, $e) = (split)[@cols];
        my $name = $#cols>2 ? (split)[$cols[3]] : 'NA';  #this is for an altName ? 
#       print STDERR join(" ",$cs, $b, $e, $name)."\n" if $debug;
#        ($b,$e) = ($b<$e) ?  ($b,$e) : ($e,$b);  # for now
        my $subseq_name = join("_",$cs, $b, $e );  # ???
        $regions{$cs}{$b} = $e;
    }
    close $INFILE;
    return \%regions;    
}
sub readCoordsFile_gff { # not really ready - don't know how to deal with seq name
    open my $INFILE, "$_[0]" or die "can't open infile $_[0]: $!";
    my %par = $_[1] ? %{$_[1]} : ();
    my $delimiter = defined( $par{'delimiter'}) ? $par{'delimiter'} : "\t";
    my $cols_str = defined($par{'cols_str'}) ? $par{'cols_str'} : '0,3,4,6,8';
    my @cols = split(',', $cols_str);
    (@cols > 4 ) or die "need >= cols for this file format, cs, b/e,b/e,strand";
    my %regions;
    while(<$INFILE>){
        chomp;
        next unless /\w/;
        my ($cs, $b, $e,$strand) = (split)[@cols];
        ($b,$e) = ($strand eq '+') ? ($b,$e) : ($e,$b);  # for now
        my $subseq_name = $#cols>3 ? (split)[$cols[4]] : join("_",$cs, $b, $e );
#       print STDERR join(" ",$cs, $b, $e, $name)."\n" if $debug;
        $regions{$cs}{$b} = $e;  # huh ? 
    }
    close $INFILE;
    return \%regions;    
}

sub revcomp{
    my $DNAin = $_[0]; 
    my $DNAout = reverse($DNAin);
    $DNAout =~ tr/ACGTacgt/TGCAtgca/;
    return $DNAout;
}

1;
