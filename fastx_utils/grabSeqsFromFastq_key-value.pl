#!/usr/bin/perl -w
use strict;
my $debug=0;
print STDERR "THIS ONLY WORKS WITH ONE-LINE-PER-SEQ FILES NOW\n";
print STDERR "filter hardcoded on key but seems easily edited\n ";
# Some fasta headers(names) are listed in a file. Read that file,
# and grab only that subset of sequences from a fasta file.
# ( where the headers/names have to match exactly )
# ONLY use this with one-line-per-seq files

sub parse_head_anno {
    my $head =shift;
    # GAAATTTAGACTGGAGG|PRCONS=IGK1_R1|PRFREQ=1.0|CONSCOUNT=1|DUPCOUNT=1  e.g.  OLD eg
    # @HWI-M01418:188:000000000-A6F8Y:1:2101:14659:21026 1:N:0:TTAGGC|SEQORIENT=F|PRIMER=IGHC2_R1_IGD|BARCODE=GGGTGCAGACAAGATCG  NEW eg
    my %head_fields =();
    my @blocks = split(/\|/,$head);
    $head_fields{'head'} = shift @blocks;
    foreach my $block (@blocks){
        my ($key,$values_list) =  (split "=",$block);
        my @values = (split ',',$values_list);
        print STDERR "key:values_list: ",$key, " ", $values_list."\t".$values[0]."\n" if $debug;
        $head_fields{$key} = $values_list ;
    }
    map{print STDERR "$_\t$head_fields{$_}\n"}keys %head_fields if $debug;
    return \%head_fields;
}

(@ARGV>=3) or die "<seqs.fq> <subset_patterns_file> <outfile> [invert?]";
 my ($infilefa, $infilesubset, $outfile) = @ARGV;
my $invert = $ARGV[3] || 0;
my $altName = 0;
my $add_length = 0;
my $filter_field = "DB"; # # # 

my $fastqin =  ( $infilefa =~ /f(ast)?q/ )+0;
my $fastqout = ( $outfile  =~ /f(ast)?q/ )+0;
my @qual_lines= $fastqin ? (3,4): ();
my $header_start = $fastqout ? "@" : ">";

open my $SUBSETFILE, $infilesubset or die "Can't open $infilesubset: $!\n";
my %subset=();
while (<$SUBSETFILE>) {
    chomp;
    next unless /\w/;
    my @F = split;
    $subset{$F[0]}=1;
}
close $SUBSETFILE;

print STDERR "keys in subset:" ."\t".join("\t",keys %subset)."---\n" if $debug;


open my $FAFILE, $infilefa or die "Can't open $infilefa: $!\n";
open my $OUTFILE, "> $outfile" or die "can't open $outfile: $!";
my $satisfies = 0;
my %head_fields=();
while( <$FAFILE>) {
    chomp(my $head = (split /\s/)[0] );
    $head = substr($head, 1);  
    %head_fields = %{parse_head_anno($head)} ; 
    print STDERR "head parsed from superset:\t" ,$head,"\n" if $debug;
    print STDERR '$head_fields{$filter_field} ', $head_fields{$filter_field}, "---\n" if $debug;
    print STDERR '$subset{$head_fields{$filter_field}} ', $subset{$head_fields{$filter_field}},"\n" if $debug;

    $satisfies =  defined( $subset{$head_fields{$filter_field}} ) ? 1 : 0 ; 
    my $seq = <$FAFILE>;

    if ( $satisfies ne $invert ) {
	chomp; 
	s/^\@/$header_start/;

	my $newhead;
	if ($altName) {
	    $newhead =  $header_start.$subset{$head} ;
	} else {
	    $newhead = $_;
	}
	if ($add_length) {
	    $newhead .= " length=". length($seq);	
	}
	print $OUTFILE $newhead ."\n";
	print $OUTFILE $seq ;
    }
    for ( @qual_lines ) {
	$_ = <$FAFILE>;
	if ( $satisfies ne $invert && $fastqout) {
	    print $OUTFILE $_ ;
	}
    }
}
close $FAFILE;
close $OUTFILE;

