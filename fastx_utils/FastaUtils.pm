#!/usr/bin/perl -w

package FastaUtils;
use strict;
use Exporter;
use vars qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);

my @fxns = qw(
read_fasta_subset
get_subseqs
revcomp
printMultiFasta
	      );

$VERSION     = 1.00;
@ISA         = qw(Exporter);
@EXPORT      = ();
@EXPORT_OK   = @fxns ;
%EXPORT_TAGS = ( DEFAULT => \@EXPORT_OK ,
                 Both    => \@EXPORT_OK ,
		 all     => \@EXPORT_OK , 
		 );

my $debug=0;


sub printFasta {
    my $nt = shift;
    my $l = (@_==1) ? $_[0] : 1000;
    $nt =~ s/([ACGT]{$l})/$1\n/ig;
    chomp($nt);
    return $nt;
}

sub printMultiFasta {
    my %mfa = %{$_[0]};
    my $fafile = shift;
    open my $OUT, "> $fafile" or die "can't open fafile $fafile: $!";
    map {
        print $OUT ">".$_."\n";
        print $OUT printFasta($mfa{$_});
        print $OUT "\n";
    } sort keys %mfa;
}
sub read_fasta_subset {
    my $fafile = shift;
    my %subset = %{$_[0]};
    open my $INFILE, "$fafile" or die "can't open fafile $fafile: $!";
    my $header = "";
    my %seqs;
    my $satisfies=0;
    while(<$INFILE>){
	chomp;
        if (s/^>//){
            $header = $_;
	    print STDERR "WARNING: sequence $header was a duplicate \n" if defined $seqs{$header};
	    $satisfies = (defined $subset{$header}) +0;
        }else{
	    $seqs{$header} .= $_ if $satisfies;
	}
    }
    # could select subseqs here before returning if memory an issue
    return \%seqs;
}


sub get_subseqs{
    my %seqs = %{$_[0]};
    my %coords = %{$_[1]};
    my %subseqs =();
    my $subseq ='';
    foreach my $head (keys %coords) {
	my ($b,$e) = @{$coords{$head}} ;
	if (grep {/NA/ }($b,$e) ){ # what SHOULD this return? 
	    $subseq = 'NA';
	    print STDERR "NA given as coord\n" if $debug;
	}elsif ( grep{$_ > length($seqs{$head}) }($b,$e)) {
	    $subseq = 'NA';
	    print STDERR "ERROR: coord $b or $e does not exist in sequence $head of length " .length($seqs{$head}) ."\n";
	}else{
	    my $len = abs($e-$b) + 1;        
	    $subseq = ($b < $e ) ? substr($seqs{$head}, $b-1, $len) : revcomp ( substr($seqs{$head}, $e-1, $len) );  # if len too much, silent
	}
	$subseqs{$head} = $subseq ;
    }
    return \%subseqs;
}




sub revcomp{
    my $DNAin = $_[0]; 
    my $DNAout = reverse($DNAin);
    $DNAout =~ tr/ACGTacgt/TGCAtgca/;
    return $DNAout;
}



1;
__END__;
