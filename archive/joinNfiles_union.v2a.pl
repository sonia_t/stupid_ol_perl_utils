#!/usr/bin/perl -w
use Getopt::Long;
use strict;
sub parse;

# # # # # # # # # # # # # # # # # # # # # # # # 
# VERSION NOTES
# looks good - tested.
# want to pad the files that don't have the right column number. 
# but i barely started mucking around
# # # # # # # # # # # # # # # # # # # # # # # # 

my $helpStr=qq{

Usage:
	join2files.pl <first inputFile> <second inputFile> < additional optional input files>> 
	
Description:
	This script takes two to four tab-delimited files and join them by the union of their identifiers.
	Output is to STDOUT: joined tab-delimited results 	 

	options: idCols:(comma delimited string)  which column the IDs are at.  start counting columns from zero!!
};


my $idColsString ;
GetOptions ( 
#             'debug!'     => \$debug ,
	     'idCols:s'   => \$idColsString ,
             );


unless (@ARGV>=2){die "$helpStr";}
my @filenames = @ARGV;

my @idCols = ( $idColsString ne '') ? split(",", $idColsString ): (map {0} @filenames);
$#idCols == $#filenames or die "unequal number of Id columns and filenames ";

my $hashR = {};
my %numcolshash;
my $numDataCols;
foreach my $filenum (0..$#filenames) {
    my $filename = $filenames[$filenum];
    my $idCol = $idCols[$filenum];
    ($hashR,$numDataCols) = parse($hashR, $filename, $idCol, $filename);
    $numcolshash{$filename} = $numDataCols;
    print STDERR $filename . "\t" . $numDataCols ."\n";
}
my %hash = %{$hashR};  # it accumulated the files' info


if (exists $hash{'header'} ) {  # if any of the files had one
    my $header .= "#id" ; 
    map{ 
	if ( exists $hash{'header'}{$_}) {
	    $header .= "\t".join("\t", @{$hash{'header'}{$_}} ) ;
	    delete $hash{'header'}{$_} ;
	} else {
	    $header .= "\tunknown" x $numcolshash{$_};
	}
    } @filenames;  
print $header."\n" if ($header ne "");  # shouldn't print unless at least one file had a header.
delete $hash{'header'};
}

map{
    my $str = $_;
    foreach my $fileid (@filenames) {
	if ( $hash{$_}{$fileid} ) { 
	    $str .= "\t".join("\t", @{$hash{$_}{$fileid}} ) ;
	} else {
	    $str .= "\tNA" x $numcolshash{$fileid}; 
#	    print STDERR $_ . "has no data in one file $fileid\n";
	}
    }
    print "$str\n";
}  keys %hash;




sub parse(){
 	my ($hashR, $file, $idCol, $name) = @_;
	my %hash = %{$hashR};
	my $numDataCols;
	open FILE, "$file" or die "Can't open $file: $!\n";
	my $l=0;
	my @temp;
	while (<FILE>){
	    chomp;
	    next if ($_ eq "");
	    @temp = split /\t/; 
	    my $id = $temp[$idCol];
	    splice(@temp, $idCol, 1);  # removes the one idCol and slides the others over stacklike
	    if ($l++ == 0 && $_ =~ /^\#/) {
		@{$hash{'header'}{$file}} = @temp;
		print STDERR "got the header\n";
		next;
	    } 
#	    $str .= "\tNA" x $numcolshash{$fileid}; 
	    @{$hash{$id}{$file}} = @temp;
#	    last if $l>4;
	    $numDataCols = @temp;
	}
	close FILE;
#	map{print STDERR join("__",@{$hash{'header'}{$_}})."\n"; } keys %{$hash{'header'}};
	return (\%hash, $numDataCols);
}


