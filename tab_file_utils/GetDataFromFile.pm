#!/usr/bin/perl -w

package GetDataFromFile;
use strict;
use Exporter;
use vars qw($VERSION @ISA @EXPORT @EXPORT_OK %EXPORT_TAGS);

my @fxns = qw( slurp_file_into_hash
               slurp_file_into_named_matrix_by_rows
	       get_list_from_file_uniqued 
	       get_list_from_file_ordered 
	       get_mapping_from_file 
	       get_matrix_from_file
	       );

$VERSION     = 1.00;
@ISA         = qw(Exporter);
@EXPORT      = ();
@EXPORT_OK   = @fxns ;
%EXPORT_TAGS = ( 
    DEFAULT => \@EXPORT_OK ,
    Both    => \@EXPORT_OK ,
    all     => \@EXPORT_OK , 
    );


sub get_matrix_from_file {  # can this deal with non-square dist ? i think i tested it ! 
    my $file = $_[0];
    open my $FILE, "< $file" or die "Can't open $file: $!";
    my $has_rowNames=1; my $has_header=1;
    my @colNames=();
    my @rowNames=(); # in order, if you want to pass it back
    my %mat;
    my @F;
    while (<$FILE>){
	chomp;
	@F=split;
	if  ( $. == 1 && $has_header) {
	    @colNames = @F;
	    next;
	}
	next unless /\w/;
	push @rowNames, $F[0];
#	@{ $mat{$F[0]} }= @F[1..$#F]; 
	$mat{$F[0]} = ();
	map{ $mat{$F[0]}{$colNames[$_]} = $F[$_]} (1..$#F);
    }
    my $num = $#F;
    close($FILE) ;
    return \%mat;
}

sub get_mapping_from_file {
    my $file =$_[0];
    my %par = $_[1] ? %{$_[1]} : ();
    my $delimiter = defined( $par{'delimiter'}) ? $par{'delimiter'} : "\t";
    my $cols_str = defined($par{'cols_str'}) ? $par{'cols_str'} : '0,1';
    my @cols = split(',', $cols_str);
    open my $FILE, "< $file" or die "Can't open $file: $!";
    my %hash;
    while(<$FILE>){
        my @F= (split)[@cols];
        $hash{$F[0]} = $F[1];
    }
    close $FILE;
    return \%hash;
}

sub slurp_file_into_hash { # args: file,idcol=0 )
    my $file = shift;
    my $id_col = defined($_[0])? $_[0] : 0;
    open my $FILE , "< ".$file  or die "Can't open $file : $!\n";
    my %h=();
    while(<$FILE>){
        chomp;
        my $k =(split "\t", $_)[$id_col];
        $h{$k}= $_ unless /^\#/;
    }
    close $FILE;
    return \%h;
}

sub slurp_file_into_named_matrix_by_rows { # args: file,idcol= line num )
    my $file = shift;
    open my $FILE , "< ".$file  or die "Can't open $file : $!\n";
    my %h=();
    my @colnames;
    while(<$FILE>){
        chomp;
	my @F = (split "\t", $_);
	if ($.==1) {
	    @colnames = @F;
	    next;
	}
        my $k = defined($_[0])? $F[$_[0]] : $.;
        %{$h{$k}}= map{$colnames[$_] => $F[$_]}(0..$#F);
        map{ $h{$k}{$colnames[$_]} = "NA"}($#F+1 .. $#colnames); # padding short rows with NAs
    }
    close $FILE;
    return \%h; # 
}
sub get_list_from_file_uniqued { # talk about redundant
    my $filename = $_[0];
    my %par = %{$_[1]} || ();
    my $delimiter = defined( $par{'delimiter'}) ? $par{'delimiter'} : "\t";
    my $col = defined($par{'col'})? $par{'col'} : 0;
    my %h = ();
    open my $INFILE,  "< $filename" or die "can't open $filename";
    while (<$INFILE>) {
	next if ($_ eq '');
	chomp; 
	my $k1 = (split /$delimiter/)[$col];
	$h{$k1}++;
    }
    close $INFILE;
    return \%h
}

sub get_list_from_file_keep_order { # talk about redundant
    my $filename = $_[0];
    my %par = %{$_[1]} || ();
    my $delimiter = defined( $par{'delimiter'}) ? $par{'delimiter'} : "\t";
    my $col = defined($par{'col'})? $par{'col'} : 0;
    my @a = ();
    open my $INFILE,  "< $filename" or die "can't open $filename";
    while (<$INFILE>) {
	next if ($_ eq '');
	chomp; 
	push @a, (split /$delimiter/)[$col];
    }
    close $INFILE;
    return \@a ;
}

1;
