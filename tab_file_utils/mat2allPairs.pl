#!/usr/bin/perl -w
use strict;

my $helpStr = " square matrix  , has header binary ";  
(@ARGV+0)==2 or die $helpStr;
my $file = $ARGV[0];
my $hasHeader = $ARGV[1];

open my $FILE, "< $file" or die "Can't open $file: $!";

my $rownames=1;
my @seqNames; # in order

my %mat;
my $l=0;
my @F;
while (<$FILE>){
    if  ( $. == 1 && $hasHeader) {
	next;
    }
    chomp;
    @F=split;
    if(/^\w/) {
	push @seqNames, $F[0];
#	@h{0..$#F}=@F;   # numbers. used this indexing somehwere else and worried about association with colnames. maybe pearson ? 
	@{ $mat{$F[0]} }= @F[1..$#F]; 
	$l++;
    }
}
my $num = $#F;
close($FILE) ;

#my %seqName2abbrev = @seqNames;
my %seqName2abbrev = map{ $_ => (/^([^_]+)_/)[0] || $_  }@seqNames if 0;
#map { $seqName2abbrev{$_} = (/(^[^_]+)_/)[0]  }@seqNames;  # works
#print STDERR join(" ", keys %seqName2abbrev, values %seqName2abbrev)."\n";

foreach my $seqname1 ( @seqNames) {
    foreach my $seqname2 (@seqNames) {
	print $seqname1 ,".vs.", $seqname2;
#	print join(".vs.", @seqName2abbrev{ $seqname1,$seqname2 }) ;
	my $dist = shift @{ $mat{$seqname1}};
	if ($seqname1  eq  $seqname2) { die "diag dist $dist not zero or 1 or NA" unless grep {$dist eq $_}(0,1,"NA")  };
	print "\t". $dist ."\n";
    }
}
