#!/usr/bin/perl -w
use strict;
sub read_mapping_file;

my $debug=0;
my $sp_pair_col= $ARGV[2] || 0 ;

@ARGV>=2 or die "file to add to, and sp2hab mapping file";
my %output = ( 'sd' => 0,
	       'habs' => 1 
	       );
my %sp2hab = %{read_mapping_file($ARGV[1])};

open my $PAIRSFILE, "< $ARGV[0]" or die "can't open $ARGV[0]";
while(<$PAIRSFILE>){
    chomp;
    next if /^\#/;

    my $sp_pair =(split)[$sp_pair_col];
    my @sp = split(".vs.",$sp_pair);
    if ( @sp < 2 ){
	@sp = split(/\./, $sp_pair);
    }

    my @h = map { $sp2hab{$_} ?   $sp2hab{$_} :  '--' }@sp;
    my $hab_string = join (".",@h) ;

    my $sd = $h[0] eq $h[1] ?  "s" : "d" ;
    $sd = ($hab_string =~ /\-\-/) ? "NA" : $sd;

    print;
    print "\t".$hab_string  if $output{'habs'};
    print "\t".$sd  if $output{'sd'};
    print "\n";
    last if $. > 5 && $debug;
}


sub read_mapping_file {
    my $metadata_file = shift;
    open my $METADATA, "< $metadata_file" or die "can't open $metadata_file";  
    my %strain2hab;
    my @cols = (0,1);
    while(<$METADATA>) {
        next if ( /^\#/ );
        chomp;
        my ($strain, $hab)= (split "\t")[@cols];
        $strain2hab{$strain} = $hab;
    }
    close $METADATA;
    return \%strain2hab;
}

__END__
