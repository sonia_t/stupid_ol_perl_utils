#!/usr/bin/perl -w
use strict;
# IMPORTS
use Getopt::Long;
sub read_mapping_file;

# DEFAULTS
my $debug=0;
my $idCols_string = '0,0';
my $dataCols_string = '0,1' ;
my $id_field_delim ;

# GETOPT
GetOptions ( 
             'debug!'     => \$debug ,
             'idCols:s'  => \$idCols_string ,
             'dataCols:s'  => \$dataCols_string ,
             'id_field_delim:s'   => \$id_field_delim , 
             ) ;
             #            'minOverlap:i'  => \$params{'minOverlap'} ,

# READ FILES 
@ARGV==2 or die "file to add to, and sp2hab mapping file, use idCols and dataCols options if necessary";
my %sp2hab = %{read_mapping_file($ARGV[1], $dataCols_string) };
my @idsCols = split(",", $idCols_string );

my $sp_col= $idsCols[0];

print STDERR "my id_col for file $ARGV[0] is $idsCols[0] \n";

# ADD COL TO FILE
open my $INFILE, "< $ARGV[0]" or die "can't open $ARGV[0]";
while(<$INFILE>){
    chomp;
    next if /^\#/;
    my $sp = (split /\t/)[$idsCols[0]];
    $sp  = (split /\Q${id_field_delim}\E/,$sp)[0] if $id_field_delim;  
#    $sp=~s/\.SSU//;  # # # # # # # # # # ##########
    my $h = $sp2hab{$sp} ? $sp2hab{$sp} : 'NA';
    print STDERR $sp."\n" if $debug;

    print;
    print "\t".$h; 
    print "\n";
    last if $. > 5 && $debug;
}
close $INFILE;


sub read_mapping_file {
    my $file = $_[0];
    my $cols_str = $_[1]? $_[1] : "0,1";
    my @cols = (split ",", $cols_str);
    open my $HABFILE, "< $file" or die "can't open $file";  
    my %data;
	while(<$HABFILE>) {
	    next if ( /^\#/ );
	    chomp;
	    my @temp  = (split "\t");
	    my $k = $temp[$cols[0]];
	    # shift @cols;
	    # my $v = join("\t", @temp[@cols[0..$#cols]]); # 
	    my $v = join("\t",map{ exists($temp[$_])? $temp[$_] : "NA"} @cols[1..$#cols] );
#	    print STDERR "$_\tv: $v\n" if $debug;
	    $data{$k}=$v;
	}
    close $HABFILE;
    return \%data;
}


__END__
