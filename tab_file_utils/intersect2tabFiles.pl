#!/usr/bin/perl -w
use Getopt::Long;

our $VERSION = "v2.1";
######## OPTIONS ##########
my $debug = 0;
my $hasHeader =0;
my $printfileAdata =0;
my $printfileBdata = 1;
my $print_not_found = 0;
my $printExcluded = 0;
my $reverseMatching = 0;
my $truncatefileBlines = 4e10 ;
my $id_field_delim = '\s';
###########################

# # # # # # # # # # # # # # # # 
# GET USER SUPPLIED PARAMETERS
if (1){
GetOptions ( 
             'debug!'     => \$debug ,
             'hasHeader!' => \$hasHeader , 
             'reverse!'  => \$reverseMatching ,
             'inverse!'  => \$reverseMatching ,
	     'process_seqid!' => \$process_seqid , 
             'id_field_delim:s'   => \$id_field_delim , 
	     'printfileAdata!' => \$printfileAdata ,
	     'print_not_found!'  => \$print_not_found ,
             ) ;
}
my $helpStr = "subset subset_col superset superset_col outfile ";
(@ARGV == 5) or die "$helpStr";
my ($infileA,$colA, $infileB, $colB , $outfile) = @ARGV;


###########################
sub process_seqid { 
    my $k1 = shift;
    $k1 =~ s/^>//;
    $k1 =~ s/\#.*//;
    $k1 =~ s/\s.*//;
#    $k1 =~ s/\|.*//;
    return $k1;
}

###########################

open INFILEA, "$infileA" or die "Can't open $infileA:$!";
open INFILEB, "$infileB" or die "Can't open $infileB:$!";
open OUTFILE, "> $outfile" or die "Can't open $outfile:$!";

my $outfile_for_excluded = $outfile . ".inverse";
if ($printExcluded) {open OUTFILE2, "> $outfile_for_excluded" or die "Can't open $outfile_for_excluded: $!"; }
 
my %h;
my $totalA;
while (<INFILEA>) {
    chomp;
    next if ($_ eq '');
    my $k1 = (split "\t")[$colA];
    $k1 = process_seqid($k1) if $process_seqid;
    $k1 = (split /\Q${id_field_delim}\E/,$k1)[0];  

    push @{$h{$k1}}, $_;   # this way the key doesn't have to be unique.
    $totalA++;
#    push @array1, $key;
}
close INFILEA;
print STDERR "keys from subset: ".join(", ",keys %h)."\n" if $debug;


my $totalB;
my $satisfying =0;
my %keys_B;
while (<INFILEB>) {
    next if ($_ eq '');
    if ($. == 1  && $hasHeader) { 
	print OUTFILE $_ ;
	next;
    }
    my $k1 = (split /[\t\n]/)[$colB];
#    my $k1 = (split "\t")[$colB];
    $k1 = process_seqid($k1) if $process_seqid;

    $k1 = (split /\Q${id_field_delim}\E/,$k1)[0];  

    $keysB{$k1}++ if $print_not_found;
    my $inSubset = defined( $h{$k1} ) +0 ;
    if ( $inSubset != $reverseMatching) {
	$satisfying++;
#	print OUTFILE $_ if $printfileBdata;   # orig
	my $temp = $h{$k1};
	foreach $line ( @$temp ) {  # commented out because this used to create the key
	    print OUTFILE $line."\t" if $printfileAdata;
	}
	print OUTFILE $_ if $printfileBdata;  # ??? line onlyused once
    } else {
	print OUTFILE2 $_ if $printfileBdata && $printExcluded ;
    }

    last if ($totalB++ > $truncatefileBlines);
}
close INFILEB;

my @not_found = grep { not defined( $keysB{$_}) } keys %h;

print STDERR "reverseMatching \t $reverseMatching"."\n";
print STDERR "stats on $infileA $infileB "."\n" ;
print STDERR $satisfying, " lines satisfying criteria"."\n" ;
print STDERR $totalA, " total lines in ", $infileA,"\n";
print STDERR $totalB, " total lines in ", $infileB,"\n";

if ($print_not_found) {
    print STDERR "keys not found in file B: \n";
    map { print STDERR join("\n", @$_) } @h{@not_found} ;
}

close OUTFILE;
#close ERRORFILE; 
